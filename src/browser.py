# -*- coding: utf-8 -*-
"""
Created on Sun Feb 16 20:36:00 2020

Web scraper for RAE.
Version 0.3

@author: Arturiki
"""
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from datetime import date, datetime, timedelta

import modes
from keys import description_classes, \
    search2beginning_link, search2end_link, definition_classes, idiom_classes


# Significado:
# Funciona perfectamente con "hola"
# Funciona perfectamente con "prueba"
# Funciona perfectamente con "probar"
# Funciona perfectamente con "palabra"
# Funciona perfectamente con "probando" (redirigiendo a "probar")

# Expresiones:
# Funciona perfectamente con "prueba"
# Funciona perfectamente con "probando" (redirigiendo a "probar")
# Funciona perfectamente con "palabra"

# Anagramas:
# Funciona perfectamente con "ala"
# Funciona perfectamente con "probando"


def get_link(the_word, the_mode):

    link = search2beginning_link[the_mode] + \
           the_word + \
           search2end_link[the_mode]

    return link


def check_date_format(the_date, the_format = "%Y-%m-d"):

    # Check and try to format as YYYY-MM-DD
    try:
        datetime.strptime(the_date, the_format)

    except ValueError:
      print(the_date + " does not follow the format " + the_format)

def compare_dates_with_today(the_date):

    check_date_format(the_date)

    today = date.today() #Formatted as YYYY-MM_DD

    try:
        the_date >= today

    except ValueError:
        print(the_date + " is part of the past.")


def check_days_is_positive_integer(the_days):

    try:
      the_days_integer = int(the_days)

      try:
          the_days_integer > 0

      except ValueError:
          print(the_days_integer + " is not a positive number.")
    except ValueError:
      print(the_days + " is not an integer.")


def get_departure_date(the_date, the_days):

    check_days_is_positive_integer(the_days)

    return the_date + timedelta(days=the_days)


def get_result(the_link, the_mode):

    try:
        the_browser = start_browser(the_link)

        modes.mode2method[the_mode](the_browser, the_mode)

        the_browser.quit()
    except NoSuchElementException:
        print('No se puede acceder la página web.')


def start_browser(the_link):

    options = webdriver.ChromeOptions()
    options.add_argument('--headless')
    browser = webdriver.Chrome(options=options)

    browser.get(the_link)
    browser.implicitly_wait(10)

    return browser


def is_in_dictionary(the_browser):

    if 'Aviso' in the_browser.find_element_by_id('resultados').text.split(':',
                                                                      1)[0]:
        return False
    else:
        return True


def print_items_or_all(the_browser):

    div_items = get_div_items(the_browser)

    if len(div_items) > 0:
        print(div_items[0].text)

    else:
        resultados = the_browser.find_element_by_id('resultados').text

        if 'Real Academia' in resultados:
            print(remove_last_line_from_string(resultados))

        else:
            print(resultados)


def get_div_items(the_element):

    return the_element.find_elements_by_class_name('item-list')


def remove_last_line_from_string(s):

    return s[:s.rfind('\n')]


def display_similar_or_no_result(the_element):
    div_items_in_list = the_element.find_elements_by_class_name('item-list')

    if len(div_items_in_list) > 0:
        print(div_items_in_list[0].text)
    else:
        print(the_element.text)


def is_correct_class(element):

    is_correct = True

    if 'MediaCard' in element.get_attribute('class'):
        is_correct = False

    return is_correct


def get_articles_and_children(the_browser, the_mode):

    articles = the_browser.find_elements_by_xpath('//article')
    good_articles = [art for art in articles if is_correct_class(art)]

    for article in good_articles:
        get_header_and_children(article, the_mode)


def get_header_and_children(article, the_mode):

    header = article.find_element_by_xpath('header')

    if 'Conjugación de ' not in header.text:
        title = header.text
        print('\n' + title)

        p_elements = article.find_elements_by_xpath('p')
        print_meaning_or_idiom(p_elements, the_mode)


def print_meaning_or_idiom(parent_element, the_mode):

    mode_classes = idiom_classes if the_mode is 'idiom' else definition_classes
    full_text = ''

    for p_element in parent_element:

        class_name = p_element.get_attribute('class')

        if (the_mode is 'meaning' and class_name in description_classes) \
                or (class_name in mode_classes):
            full_text = p_element.text
            print(full_text)

        else:
            pass

    if full_text == '':
        print('No hay coincidencias.')
